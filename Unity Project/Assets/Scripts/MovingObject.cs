﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]

public class MovingObject : MonoBehaviour
{
    GameManager gameManager;
    public Queue<SurfaceRecord> SurfaceRecords = new Queue<SurfaceRecord>();
    public Vector3 StartPosition;

    protected ParticleSystem particleSystem;
    protected GameObject startPlatform;
    protected bool isOnStartPlatform = false;

    protected void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        rigidbody2D.gravityScale = 0f;
        rigidbody2D.fixedAngle = true;
        particleSystem = GetComponentInChildren<ParticleSystem>();
        particleSystem.enableEmission = false;
        startPlatform = (GameObject)GameObject.Find("StartPlatform(Clone)");
    }

    public void OnLevelReady()
    {
        StartPosition = ((GameObject)GameObject.FindGameObjectWithTag("StartPosition")).transform.position;
        //StartPosition = new Vector3(StartPosition.x, StartPosition.y, StartPosition.z - 1f);
        transform.position = StartPosition;
    }

    public void Reset()
    {
        SurfaceRecords.Clear();
        rigidbody2D.velocity = Vector2.zero;
        transform.position = StartPosition;
    }

    public void Launch()
    {
        if (isOnStartPlatform)
        {
            rigidbody2D.velocity = Vector2.right * Level.AllLevels[Level.CurrentLevelIndex].InitialVelocity;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //this.transform.position = new Vector3(transform.position.x, transform.position.y, StartPosition.z);
        if (rigidbody2D.velocity.x > 0 && SurfaceRecords.Count > 0)
        {
            SurfaceRecord record = SurfaceRecords.Peek();
            if (record.IsOnSurface())
            {
                //print(string.Format("Vel(i): {0}, Vel(c): {1}, Dist: {2}, Length: {3}, Vel(n): {4}, Vel(f): {5}",
                //    record.InitialVelocity.x, rigidbody2D.velocity.x, record.GetRelativePosition(), 
                //    record.Length, record.GetNewVelocity().x, record.FinalVelocity.x));
                rigidbody2D.velocity = record.GetNewVelocity();
                particleSystem.enableEmission = true;
            }
            else
            {
                particleSystem.enableEmission = false;
            }
        }
        else
        {
            particleSystem.enableEmission = false;
        }
    }

    protected float calculateFrictionForce(float coefficient)
    {
        return rigidbody2D.mass * Mathf.Abs(Physics2D.gravity.y) * coefficient;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Surface>() != null)
        {
            //if (other.collider2D.OverlapPoint(this.transform.position))
            {
                enterNewSurface(other.GetComponent<Surface>());
            }
        }
        else if (other.GetComponent<Teleporter>() != null)
        {
            other.GetComponent<Teleporter>().TryTeleport(this.gameObject);
        }
        else if (other.name == "StartPlatform(Clone)")
        {
            isOnStartPlatform = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.GetComponent<Surface>() != null)
        {
            Surface surface = other.GetComponent<Surface>();
            if (SurfaceRecords.Count > 0 && surface == SurfaceRecords.Peek().Surface)
            {
                exitCurrentSurface();

            }
            print(string.Format("Current Velocity: {0}", rigidbody2D.velocity.x));
            //print("Exit");

        }
        else if (other.name == "StartPlatform(Clone)")
        {
            isOnStartPlatform = false;
        }
        //print(string.Format("LeftEdge: {0}, Length: {1}, Distance: {2}", 
        //    contactSurface.LeftEdge, contactSurface.Length, transform.position.x - contactSurface.LeftEdge));
    }

    protected void enterNewSurface(Surface surface)
    { 
        Vector2 initialVelocity = rigidbody2D.velocity;
        if (SurfaceRecords.Count > 0)
        {
            initialVelocity = SurfaceRecords.Peek().FinalVelocity;
        }
        SurfaceRecord record = new SurfaceRecord(this, initialVelocity, surface);
        SurfaceRecords.Enqueue(record);
        particleSystem.emissionRate = record.Surface.Coefficient * 80f;
        particleSystem.startSize = record.Surface.Coefficient * 0.8f + 0.2f;
        //print(record.FinalVelocity);
    }

    protected void exitCurrentSurface()
    { 
        if (SurfaceRecords.Count > 0)
        { 
            rigidbody2D.velocity = SurfaceRecords.Dequeue().FinalVelocity;
        }
    }
}
