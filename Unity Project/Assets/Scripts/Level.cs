﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;


public class Level
{
    public List<string> Paths;
    public int PathCount;
    public int SurfaceCount;
    public int InitialVelocity;
    public float RequiredFrictionCoefficient;

    protected Level() { }

    protected Level(List<string> paths, int pathCount, int surfaceCount, 
        int initialVelocity, float requiredFrictionCoefficient)
    {
        this.Paths = paths;
        this.PathCount = pathCount;
        this.SurfaceCount = surfaceCount;
        this.InitialVelocity = initialVelocity;
        this.RequiredFrictionCoefficient = requiredFrictionCoefficient;
    }







    public static List<Level> AllLevels = new List<Level>();
    public static int CurrentLevelIndex = 0;

    static Level()
    {
        string filePath = Application.dataPath;
        filePath += "/Data/Levels.xml";

        XElement doc = XElement.Load(filePath);
        List<XElement> levels = new List<XElement>(doc.Descendants("level"));

        foreach (XElement level in levels)
        {
            int initialVelocity = int.Parse(level.Element("initialVelocity").Value);
            float totalCoefficient = float.Parse(level.Element("totalCoefficient").Value);
            string layout = level.Element("layout").Value;
            List<string> paths = new List<string>(layout.Split(','));
            int pathCount = paths.Count;
            int surfaceCount = paths[0].Length;
            if (pathCount >= 2 && pathCount <= 5
                && surfaceCount >= 3 && surfaceCount <= 6)
            {
                AllLevels.Add(new Level(paths, pathCount, surfaceCount, initialVelocity, totalCoefficient));
            }
        }
    }

    protected static GameObject pathPrefab = (GameObject)Resources.Load("Path");
    protected static GameObject surfacePrefab = (GameObject)Resources.Load("Surface");
    protected static GameObject teleporterPrefab = (GameObject)Resources.Load("Teleporter");
    protected static GameObject startPosPrefab = (GameObject)Resources.Load("StartPosition");
    protected static GameObject movingObjectPrefab = (GameObject)Resources.Load("MovingObject");
    protected static GameObject startPlatformPrefab = (GameObject)Resources.Load("StartPlatform");
    protected static GameObject goalPlatformPrefab = (GameObject)Resources.Load("GoalPlatform");

    //protected static readonly float PATH_LENGTH = 19f;
    protected static readonly float PATH_SEPARATION = 2.5f;
    protected static readonly float PATH_Z_POS = 0F;

    protected static readonly float SURFACE_LENGTH = 4f;
    protected static readonly float SURFACE_SEPARATION = 1.5f;
    protected static readonly float SURFACE_Z_POS = -2F;

    //protected static readonly float TELEPORTER_RADIUS = 1.5f;
    protected static readonly float TELEPORTER_Z_POS = -3F;

    protected static readonly float START_POS_LENGTH = 1f;
    protected static readonly float START_POS_Z_POS = -3F;

    public static void InstantiateLevel(int levelIndex)
    {
        InstantiateLevel(AllLevels[levelIndex]);
    }

    public static void InstantiateLevel(Level level)
    {
        float totalPathSeparation = (level.PathCount - 1) * PATH_SEPARATION;
        float firstPathYPos = totalPathSeparation / 2f;
        float pathLength = level.Paths[0].Length * SURFACE_LENGTH 
            + (level.Paths[0].Length - 1) * SURFACE_SEPARATION;

        for (int i = 0; i < level.PathCount; i++)
        {
            float pathYPos = firstPathYPos - i * PATH_SEPARATION;
            InstantiatePath(level.Paths[i], pathYPos, pathLength);
        }

        InstantiatePlatforms(totalPathSeparation, pathLength);
    }

    public static void InstantiatePath(string pathString, float pathYPos, float pathLength)
    {
        GameObject pathObject = (GameObject)GameObject.Instantiate(pathPrefab);
        pathObject.transform.localScale = 
            new Vector3(pathLength, pathObject.transform.localScale.y, pathObject.transform.localScale.z);
        pathObject.transform.position = new Vector3(0f, pathYPos, 0f);

        float firstSurfaceXPos = (-1 * pathLength + SURFACE_LENGTH) / 2;

        for (int i = 0; i < pathString.Length; i++)
        {
            float surfaceXPos = firstSurfaceXPos + i * (SURFACE_SEPARATION + SURFACE_LENGTH);
            InstantiateSurface(pathString[i].ToString(), surfaceXPos, pathYPos);

            if (i < pathString.Length - 1)
            {
                float teleporterXPos = surfaceXPos + (SURFACE_SEPARATION + SURFACE_LENGTH) / 2f;
                InstantiateTeleporter(teleporterXPos, pathYPos);
            }
        }

        float startPosXPos = (-1 * pathLength - START_POS_LENGTH) / 2 - 0.1f;
        InstantiateStartPos(startPosXPos, pathYPos);
    }

    public static void InstantiateSurface(string surfaceType, float surfaceXPos, float surfaceYPos)
    {
        GameObject surfaceObject = (GameObject)GameObject.Instantiate(surfacePrefab);
        Surface surface = surfaceObject.GetComponent<Surface>();
        foreach (SurfaceType enumValue in SurfaceType.GetValues(typeof(SurfaceType)))
        {
            if (string.Compare(surfaceType, (enumValue.ToString())[0].ToString(), true) == 0)
            {
                surface.SetSurface(enumValue);
            }
        }
        surfaceObject.transform.position =
            new Vector3(surfaceXPos, surfaceYPos, SURFACE_Z_POS);
        ((BoxCollider2D)surfaceObject.collider2D).size = new Vector2(4, 1);
    }

    public static void InstantiateTeleporter(float teleporterXPos, float teleporterYPos)
    {
        GameObject teleporterObject = (GameObject)GameObject.Instantiate(teleporterPrefab);
        teleporterObject.transform.position =
            new Vector3(teleporterXPos, teleporterYPos, TELEPORTER_Z_POS);
    }

    public static void InstantiateStartPos(float startPosXPos, float startPosYPos)
    {
        GameObject startPosObject = (GameObject)GameObject.Instantiate(startPosPrefab);
        startPosObject.transform.position =
            new Vector3(startPosXPos, startPosYPos, START_POS_Z_POS);
    }

    public static void InstantiatePlatforms(float totalPathSeparation, float pathLength)
    {
        GameObject platform = (GameObject)GameObject.Instantiate(startPlatformPrefab);
        float height = totalPathSeparation + GameObject.FindGameObjectWithTag("Path").transform.localScale.y;
        platform.transform.localScale =
            new Vector3(platform.transform.localScale.x, height, platform.transform.localScale.z);
        platform.transform.position =
            new Vector3(-1 * (pathLength + START_POS_LENGTH) / 2, 0f, PATH_Z_POS);

        platform = (GameObject)GameObject.Instantiate(goalPlatformPrefab);
        platform.transform.localScale =
            new Vector3(platform.transform.localScale.x, height, platform.transform.localScale.z);
        platform.transform.position =
            new Vector3((pathLength + START_POS_LENGTH) / 2, 0f, PATH_Z_POS);
    }
}
