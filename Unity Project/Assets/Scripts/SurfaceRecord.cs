﻿using UnityEngine;
using System.Collections;

public class SurfaceRecord
{
    protected const float GRAVITY = -10F;

    public MovingObject MovingObject { get; set; }
    public Surface Surface { get; set; }
    public Vector2 InitialVelocity { get; set; }
    public Vector2 FinalVelocity { get; set; }
    public float LeftEdge { get; set; }
    public float Length { get; set; }

    public SurfaceRecord(MovingObject movingObject, Vector2 initialVelocity, Surface surface)
    {
        this.MovingObject = movingObject;
        this.Surface = surface;

        this.Length = surface.GetComponent<SpriteRenderer>().bounds.size.x;
        this.LeftEdge = surface.transform.position.x - Length / 2;

        this.InitialVelocity = initialVelocity;
        this.FinalVelocity = calculateFinalVelocity();
        //print("FinalVelocity: " + FinalVelocity);
    }

    public static float CalculateVelocity(float initialVelocity, float coefficient, float distance)
    {
        float acceleration = -1 * Mathf.Abs(coefficient * GRAVITY);
        //print("Acceleration: " + acceleration);
        float velocitySquared = Mathf.Pow(initialVelocity, 2) + 2 * acceleration * distance;
        return (velocitySquared > 0f) ? Mathf.Sqrt(velocitySquared) : 0f;
    }

    public static float CalculateVelocity(float initialVelocity, float coefficient, float distance, ref string numbers)
    {
        float acceleration = -1 * Mathf.Abs(coefficient * GRAVITY);
        numbers += "initial velocity: " + initialVelocity + "\n";
        numbers += "acceleration: " + acceleration + "\n";
        numbers += "distance: " + distance + "\n";
        numbers += "coefficient: " + coefficient + "\n";
        //print("Acceleration: " + acceleration);
        float velocitySquared = Mathf.Pow(initialVelocity, 2) + 2 * acceleration * distance;
        numbers += "velocitySquared: " + velocitySquared + "\n";
        numbers += "Final Velocity: " + ((velocitySquared > 0f) ? Mathf.Sqrt(velocitySquared) : 0f) + "\n";
        return (velocitySquared > 0f) ? Mathf.Sqrt(velocitySquared) : 0f;
    }

    public Vector2 GetNewVelocity()
    {
        float distance = GetRelativePosition();
        float velocity = CalculateVelocity(InitialVelocity.x, Surface.Coefficient, distance);
        return new Vector2(velocity, 0f);
    }

    protected Vector2 calculateFinalVelocity()
    {
        float velocity = CalculateVelocity(InitialVelocity.x, Surface.Coefficient, Length);
        return new Vector2(velocity, 0f);
    }



    public float GetRelativePosition()
    {
        return (MovingObject.transform.position.x - MovingObject.transform.localScale.x / 2f) - LeftEdge;
    }

    public bool IsOnSurface()
    {
        return GetRelativePosition() > 0f;
    }
}
