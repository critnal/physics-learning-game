﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectableObject : MonoBehaviour
{
    public static class Visibility
    {
        public static readonly float Hide = 0f;
        public static readonly float Show = 0.2f;
        public static readonly float Hold = 1.0f;
        public static readonly float ShowDistance = 2f;
    }
    public static readonly List<SelectableObject> AllObjects = new List<SelectableObject>();

    void Start()
    {
        AllObjects.Clear();
        AllObjects.AddRange(GameObject.FindObjectsOfType<SelectableObject>());

    }

    protected float alpha = 0f;
    protected bool beingHeld = false;

    public virtual void UpdateAlpha(bool isCloseToMouse)
    {
        if (beingHeld)
        {
            SetAlpha(Visibility.Hold);
        }
        else if (isCloseToMouse)
        {
            SetAlpha(Visibility.Show);
        }
        else
        {
            SetAlpha(Visibility.Hide);
        }
    }

    public void SetAlpha(float alpha)
    {
        this.renderer.material.color =
            new Color(
                this.renderer.material.color.r,
                this.renderer.material.color.g,
                this.renderer.material.color.b,
                alpha);
    }

    public void HoldDown()
    {
        beingHeld = true;
    }

    public void Release()
    {
        beingHeld = false;
    }
}
