﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Teleporter : SelectableObject
{
    public static List<Teleporter> AllTeleporters = new List<Teleporter>();
    protected static float gap;

    public int ID;
    protected Teleporter Target;
    protected LineRenderer lineRenderer = null;
    protected GameObject teleportEffect;

    // Use this for initialization
    void Start()
    {
        AllTeleporters.AddRange(GameObject.FindObjectsOfType<Teleporter>());
        setTeleporterIDs();
        setTeleporterGap();

        lineRenderer = this.GetComponent<LineRenderer>();
        lineRenderer.SetVertexCount(2);
        lineRenderer.SetPosition(0, this.transform.position);
        Target = this;

        teleportEffect = (GameObject)Resources.Load("TeleportEffect");
    }

    // Update is called once per frame
    void Update()
    {
        drawLine();
    }

    public override void UpdateAlpha(bool isCloseToMouse)
    {
        if (beingHeld || Target != this)
        {
            SetAlpha(Visibility.Hold);
        }
        else if ((Controls.HoldingObject != null && Controls.HoldingObject.GetType() == typeof(Teleporter) && IsValidPair(this, (Teleporter)Controls.HoldingObject))
            || (Controls.HoldingObject == null && isCloseToMouse))
        {
            SetAlpha(Visibility.Show);
        }
        else
        {
            SetAlpha(Visibility.Hide);
        }
    }

    protected void drawLine()
    {
        if (beingHeld)
        {
            //print("1");
            lineRenderer.enabled = true;
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos = new Vector3(mousePos.x, mousePos.y, this.transform.position.z);
            lineRenderer.SetPosition(1, mousePos);
        }
        else if (Target != this && Target != null)
        {
            //print("2");
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(1, Target.transform.position);
        }
        else
        {
            lineRenderer.enabled = false;
        }
        
    }

    public void TrySetTarget(Teleporter target)
    {
        if (IsValidPair(this, target))
        {
            this.Target = target;
        }
    }

    public void ResetTarget()
    {
        Target = this;
    }

    public void TryTeleport(GameObject gmObject)
    {
        if (Target != null && Target != this)
        {
            StartCoroutine(doTeleport(gmObject));            
        }
    }

    protected IEnumerator doTeleport(GameObject gmObject)
    {
        doTeleportEffect(this.transform.position);
        gmObject.transform.position = new Vector3(100f, 100f, 100f);
        yield return new WaitForSeconds(0.3f);
        gmObject.transform.position = Target.transform.position;
        doTeleportEffect(gmObject.transform.position);
    }

    protected void doTeleportEffect(Vector3 location)
    {
        Vector3 pos = new Vector3(location.x,location.y, location.z - 1f);
        GameObject effect = (GameObject)GameObject.Instantiate(teleportEffect,pos, Quaternion.identity);
        Destroy(effect, 2f);
    }

    protected static void setTeleporterGap()
    {
        Teleporter[] teleporters = GameObject.FindObjectsOfType<Teleporter>();
        if (teleporters.Length > 2)
        {
            float shortestDistance = 99999f;
            for (int i = 1; i < teleporters.Length; i++)
            {
                float currentDistance = Vector3.Distance(teleporters[0].transform.position, teleporters[i].transform.position);
                if (currentDistance < shortestDistance)
                {
                    shortestDistance = currentDistance;
                }
            }
            gap = shortestDistance;
        }
    }

    protected static void setTeleporterIDs()
    {
        for (int i = 0; i < SelectableObject.AllObjects.Count; i++)
        {
            if (SelectableObject.AllObjects[i].GetType() == typeof(Teleporter))
            {
                ((Teleporter)SelectableObject.AllObjects[i]).ID = i;
            }
        }
    }

    public static bool IsValidPair(Teleporter origin, Teleporter target)
    {
        return areAlignedVertically(origin, target) && doesNotCreateLoop(origin, target);
    }

    protected static bool areAlignedVertically(Teleporter origin, Teleporter target)
    {
        float horizontalDistance = Mathf.Abs(origin.transform.position.x - target.transform.position.x);
        return horizontalDistance < gap / 2f;
    }

    public static bool doesNotCreateLoop(Teleporter origin, Teleporter current)
    {
        if (current.Target == current)
        {
            return true;
        }
        else if (current.Target == origin) 
        {
            return false;
        }
        else
        {
            return doesNotCreateLoop(origin, current.Target);
        }
    }

    public static void ResetAll()
    {
        AllTeleporters.ForEach(t => t.ResetTarget());
    }
}
