﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Button : MonoBehaviour
{
    public string Label { get; set; }

    protected void Start()
    {
        if (gameObject.name.Contains("Button="))
        {
            Label = gameObject.name.Split('=')[1];
        }
    }
    
    protected void OnGUI()
    {
        GUIStyle labelStyle = new GUIStyle();
        labelStyle.alignment = TextAnchor.MiddleCenter;
        labelStyle.normal.textColor = Color.white;
        labelStyle.fontSize = 18;

        Vector3 objectScreenPosition = Camera.main.WorldToScreenPoint(this.transform.position);
        float objectLabelSize = 200f;
        Rect objectLabelRect = new Rect(
            objectScreenPosition.x - objectLabelSize / 2,
            Screen.height - objectScreenPosition.y - objectLabelSize / 2,
            objectLabelSize,
            objectLabelSize);
        GUI.Label(objectLabelRect, Label, labelStyle);
    }
}
