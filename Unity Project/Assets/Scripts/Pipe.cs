﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Path
{
    public List<Surface> Surfaces;

    public Path(List<Surface> surfaces)
    {
        this.Surfaces = surfaces;
    }
}
