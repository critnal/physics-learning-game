﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            GameObject clickedObject = Controls.GetObjectUnderMouse();

            if (clickedObject != null)
            {
                if (clickedObject.tag == "GUI")
                {
                    if (clickedObject.name.Contains("Button="))
                    {
                        Level.CurrentLevelIndex = int.Parse(clickedObject.name.Split(' ')[1]) - 1;
                        Application.LoadLevel("Level");
                    }
                }
            }
        }
    }


}
