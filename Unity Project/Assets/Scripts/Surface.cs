﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum SurfaceType
{
    Wood, Rubber, Ice, Metal, Gravel
}

[RequireComponent(typeof(BoxCollider2D))]

public class Surface : MonoBehaviour
{
    protected static readonly Dictionary<SurfaceType, float> coefficients = new Dictionary<SurfaceType, float>()
    {
        { SurfaceType.Wood, 0.25f },
        { SurfaceType.Rubber, 0.7f },
        { SurfaceType.Ice, 0.05f },
        { SurfaceType.Metal, 0.4f },
        { SurfaceType.Gravel, 0.5f }
    };

    public SurfaceType SurfaceType { get; protected set; }
    public float Coefficient { get { return coefficients[SurfaceType]; } }

    //void Start()
    //{
    //    this.Coefficient = coefficients[this.GetComponent<SpriteRenderer>().sprite.name];
    //    //print(Coefficient);
    //}

    public void SetSurface(SurfaceType surfaceType)
    {
        this.SurfaceType = surfaceType;
        this.GetComponent<SpriteRenderer>().sprite = (Sprite)Resources.Load("Sprites/" + surfaceType.ToString(), typeof(Sprite));
    }

    protected void OnGUI()
    {
        drawCoefficient();
    }

    private void drawCoefficient()
    {
        GUIStyle labelStyle = new GUIStyle();
        labelStyle.alignment = TextAnchor.MiddleCenter;
        labelStyle.normal.textColor = Color.white;
        labelStyle.fontSize = 20;

        Vector3 objectScreenPosition = Camera.main.WorldToScreenPoint(this.transform.position);
        string objectLabel = Coefficient.ToString();
        float objectLabelSize = 200f;
        Rect objectLabelRect = new Rect(
            objectScreenPosition.x - objectLabelSize / 2,
            Screen.height - objectScreenPosition.y - objectLabelSize / 2,
            objectLabelSize,
            objectLabelSize);
        GUI.Label(objectLabelRect, objectLabel, labelStyle);
    }
}
