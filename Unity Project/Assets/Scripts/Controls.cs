﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controls : MonoBehaviour
{
    public static SelectableObject HoldingObject;
    public static bool TouchingGUI = false;
    protected GameManager gameManager;
    protected MovingObject movingObject;

    void Start()
    {
        gameManager = GameManager.FindObjectOfType<GameManager>();
        movingObject = GameObject.FindObjectOfType<MovingObject>();
        HoldingObject = null;
    }

    void Update()
    {
        showCloseSelectableObjects();
        handleUserClick();
    }

    protected void showCloseSelectableObjects()
    {
        Vector2 v2 = Input.mousePosition;
        Vector3 v3 = new Vector3(v2.x, v2.y, 0f);
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(v3);

        foreach (SelectableObject selectableObject in SelectableObject.AllObjects)
        {
            bool isCloseToMouse = getDistanceXY(worldPos, selectableObject.transform.position) < SelectableObject.Visibility.ShowDistance;
            selectableObject.UpdateAlpha(isCloseToMouse);

        }
    }

    protected float getDistanceXY(Vector3 a, Vector3 b)
    {
        return Vector2.Distance(new Vector2(a.x, a.y), new Vector2(b.x, b.y));
    }

    protected void handleUserClick()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            GameObject clickedObject = GetObjectUnderMouse();
            if (clickedObject == null)
            {
                return;
            }
            if (clickedObject.tag == "GUI")
            {
                TouchingGUI = true;
            }
            else if (clickedObject.GetComponent<SelectableObject>() != null)
            {
                SelectableObject hObject = clickedObject.GetComponent<SelectableObject>();
                hObject.HoldDown();
                HoldingObject = clickedObject.GetComponent<SelectableObject>();
            }
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            GameObject.Find("HelpWindow").GetComponent<SpriteRenderer>().enabled = false;
            GameObject clickedObject = GetObjectUnderMouse();
            if (clickedObject != null)
            {
                if (TouchingGUI)
                {
                    if (clickedObject.tag == "GUI")
                    {
                        handleButtonClick(clickedObject);
                    }
                }
            }

            if (HoldingObject != null)
            {
                HoldingObject.Release();
                if (clickedObject != null)
                {
                    if ((HoldingObject.GetType() == typeof(Teleporter))
                        && clickedObject.GetComponent<Teleporter>() != null)
                    {
                        Teleporter target = clickedObject.GetComponent<Teleporter>();
                        ((Teleporter)HoldingObject).TrySetTarget(target);
                    }
                    else if (clickedObject.tag == "StartPosition"
                        && clickedObject.GetComponent<SelectableObject>() != null
                        && clickedObject.GetComponent<SelectableObject>() == HoldingObject)
                    {
                        movingObject.Reset();
                        movingObject.transform.position = clickedObject.transform.position;
                        movingObject.StartPosition = clickedObject.transform.position;
                    }
                }
            }

            HoldingObject = null;
            TouchingGUI = false;
        }
    }

    public static GameObject GetObjectUnderMouse()
    {
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Physics2D.OverlapPoint(worldPos)!= null)
        {
            return Physics2D.OverlapPoint(worldPos).gameObject;
        }
        return null;
    }

    protected void handleButtonClick(GameObject button)
    {
        if (button.name == "LaunchButton")
        {
            movingObject.Launch();
        }
        else if (button.name == "ResetBlockButton")
        {
            gameManager.ResetBlock();
        }
        else if (button.name == "ResetTeleportersButton")
        {
            gameManager.ResetTeleporters();
        }
        else if (button.name == "BackButton")
        {
            //HoldingObject = null;
            Application.LoadLevel("MainMenu");
        }

        if (button.name == "HelpButton")
        {
            GameObject.Find("HelpWindow").GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    void OnGUI()
    {
        Rect buttonRect = new Rect(0f, Screen.height / 30, Screen.width, Screen.height / 10);
        GUILayout.BeginArea(buttonRect);
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                Level level = Level.AllLevels[Level.CurrentLevelIndex];
                GUILayout.Label(string.Format("Initial Velocity: {0}, Required Total Friction Coefficient: {1}",
                    level.InitialVelocity, level.RequiredFrictionCoefficient));
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.EndArea();
        
    }
}
