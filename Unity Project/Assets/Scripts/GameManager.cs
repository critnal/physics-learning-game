﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    MovingObject movingObject;
    GameObject goalPlatform;

    Sprite goalPlatformRed;
    Sprite goalPlatformGreen;
    public bool IsLevelComplete = false;

    void OnLevelWasLoaded(int level)
    {
        Level.InstantiateLevel(Level.CurrentLevelIndex);
        movingObject = GameObject.FindObjectOfType<MovingObject>();
        movingObject.OnLevelReady();
        goalPlatform = (GameObject)GameObject.Find("GoalPlatform(Clone)");
        goalPlatformRed = (Sprite)Resources.Load("Sprites/goal platform red", typeof(Sprite));
        goalPlatformGreen = (Sprite)Resources.Load("Sprites/goal platform green", typeof(Sprite));
    }

    public void LoadLevel(int level)
    {
        
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (goalHasBeenAchieved())
        {
            LevelGoalAchieved();
        }
        else
        {
            LevelGoalNotAchieved();
        }
    }

    protected bool goalHasBeenAchieved()
    {
        return Mathf.Abs(movingObject.transform.position.x - goalPlatform.transform.position.x) <= 0.2f
            && movingObject.rigidbody2D.velocity.x <= 0;
    }

    public void LevelGoalAchieved()
    {
        //if (!IsLevelComplete)
        {
            IsLevelComplete = true;
            goalPlatform.GetComponent<SpriteRenderer>().sprite = goalPlatformGreen;
        }
    }

    public void LevelGoalNotAchieved()
    {
        //if (IsLevelComplete)
        {
            IsLevelComplete = false;
            goalPlatform.GetComponent<SpriteRenderer>().sprite = goalPlatformRed;
        }
    }

    public void ResetBlock()
    {
        movingObject.Reset();
        IsLevelComplete = false;
        goalPlatform.GetComponent<SpriteRenderer>().sprite = goalPlatformRed;
    }

    public void ResetTeleporters()
    {
        Teleporter.ResetAll();
    }
}
